package com.example.rodrigokirschner.sendorder;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    int text=0;


    public void botaoTeste(View v){
        Button btTeste = (Button) findViewById(R.id.btSolicitarPedido);
        CheckBox primeiro = (CheckBox)findViewById(R.id.primeiroCheck);
        CheckBox segundo = (CheckBox)findViewById(R.id.segundoCheck);
        System.out.println("O resultado é:"+text);
        if(primeiro.isChecked()){
            System.out.println("Primeiro "+ pegaString());
        }
         if(segundo.isChecked()){
            System.out.println("Segundo "+ pegaString());
        }
        if(!segundo.isChecked()&&!primeiro.isChecked()){
            System.out.println("Nada a mais "+pegaString());
        }

    }


    public String pegaString(){
        EditText entrada= (EditText)findViewById(R.id.entrada);
        return entrada.getText().toString();
    }


    @Override
    public void onClick(View v) {
        TextView valor = (TextView)findViewById(R.id.numero);
        int resultado=Integer.parseInt(valor.getText().toString());
        switch (v.getId()){
            case R.id.aumenta:
                resultado++;
                break;
            case R.id.reduz:
                resultado--;
                break;
        }
        text=resultado;
        valor.setText(resultado+"");
    }
}
